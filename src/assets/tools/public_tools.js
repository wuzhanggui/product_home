function start_to_end(start=0,end=0,time = 100){  //从开始的位置移动到结束的位置
    let timer  = null;
    cancelAnimationFrame(timer);
    //获取当前毫秒数
    let startTime = +new Date();     
    //获取当前页面的滚动高度
    let b = start;
    let d = time;
    let c = start - end;
    timer = requestAnimationFrame(function func(){
        let t = d - Math.max(0,startTime - (+new Date()) + d);
        document.documentElement.scrollTop = document.body.scrollTop = t * (-c) / d + b;
        timer = requestAnimationFrame(func);
        if(t == d){
            cancelAnimationFrame(timer);
        }
    });
}

export {
    start_to_end,
}