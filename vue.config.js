const path = require('path')

// 在vue-config.js 中加入
// 开启gzip压缩
const CompressionWebpackPlugin = require('compression-webpack-plugin');
// 判断开发环境
const isProduction = process.env.NODE_ENV === 'production';

const resolve = dir => {
    return path.join(__dirname, dir);
}
const BASE_URL = process.env.NODE_ENV === 'production' ? '/' : '/';

module.exports = {
    /* 部署生产环境和开发环境下的URL：可对当前环境进行区分，baseUrl 从 Vue CLI 3.3 起已弃用，要使用publicPath */
    publicPath: BASE_URL,
    /* 输出文件目录：在npm run build时，生成文件的目录名称 */
    outputDir: 'dist',
    /* 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录 */
    assetsDir: "static",
    /* 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度 */
    productionSourceMap: false,
    /* 默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存，你可以通过将这个选项设为 false 来关闭文件名哈希。(false的时候就是让原来的文件名不改变) */
    filenameHashing: false,
    /* 代码保存时进行eslint检测 */
    lintOnSave: true,
    //webpack配置
    configureWebpack:config => {
        
        if (isProduction) {
            // 开启gzip压缩
            config.plugins.push(new CompressionWebpackPlugin({
                algorithm: 'gzip',
                test: /\.js$|\.html$|\.json$|\.css/,
                threshold: 10240,
                minRatio: 0.8,
                deleteOriginalAssets: false, //是否删除原文件
            }));
            // 开启分离js
            // config.optimization = {
            //     runtimeChunk: 'single',
            //     splitChunks: {
            //     chunks: 'all',
            //     maxInitialRequests: Infinity,
            //     minSize: 20000,
            //     cacheGroups: {
            //         vendor: {
            //         test: /[\\/]node_modules[\\/]/,
            //         name (module) {
            //             // get the name. E.g. node_modules/packageName/not/this/part.js
            //             // or node_modules/packageName
            //             const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
            //             // npm package names are URL-safe, but some servers don't like @ symbols
            //             return `npm.${packageName.replace('@', '')}`
            //         }
            //         }
            //     }
            //     }
            // };
            // 取消webpack警告的性能提示
            config.performance = {
                hints:'warning',
                    //入口起点的最大体积
                    maxEntrypointSize: 50000000,
                    //生成文件的最大体积
                    maxAssetSize: 30000000,
                    //只给出 js 文件的性能提示
                    assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js');
                }
            };
        }
    },
    devServer: {
        host: 'localhost',
        port: 8080, // 端口号 
        hotOnly: false,
        https: false, // https:{type:Boolean}
        open: false, //配置自动启动浏览器
        proxy:null // 配置跨域处理,只有一个代理
    },
    chainWebpack: config => {
        config.resolve.alias
        .set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
        .set('@c', resolve('src/components'));
        config.plugins.delete('prefetch');
    },
}
